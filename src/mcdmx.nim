import
  std/[os, options, tables],
  pkg/gram,
  ./scrapping,
  ./paths,
  ./metro

proc main() =
  if paramCount() < 2:
    stderr.writeLine("Usage: ./mcdmx <source> <target>")
    quit 1

  const
    kmlName = "../assets" / "Metro_CDMX.kml"
    metro = kmlName.scrapMetroSystem()

  let
    graph = metro.buildGraph()
    source = paramStr(1)
    target = paramStr(2)

  if source notin metro.stations or target notin metro.stations:
    stderr.writeLine("Invalid metro stations")
    quit 1

  let path = graph.bfs(source, target)

  if path.isSome:
    let p = get path

    var route = initRoute()
    for s in p:
      route.add(metro.stations[s])

    for s in route:
      echo s

  else:
    stderr.writeLine("No path found...")
    quit 1

when isMainModule:
  main()
